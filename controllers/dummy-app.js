
const User = require('../models/user')

// login => GET
module.exports.getLogin = (req, res, next) => {
    res.render('login', {
        message: ''
    })
}

// signup => GET
module.exports.getSignup = (req, res, next) => {
    res.render('signup', {
        message: ''
    })
}

// home => GET
module.exports.getHome = (req, res, next) => {
    res.render('home')
}

// login => POST
module.exports.login = (req, res, next) => {
    const username = req.body.username
    const password = req.body.password

    User.findOne({'email': username, 'password': password})
        .then(user => {
            // console.log(user.firstName)
            user.loginCount = user.loginCount + 1
            console.log(user.loginCount)
            return user.save()
        })
        .then(result => {
            res.render('home', {
                username: result.firstName,
                loginCount: result.loginCount
            })
        })
        .catch(error => {
            console.log(error)
            res.render('login', {
                message: 'Invalid Credentials...'
            })
        })
}

// signup => POST
module.exports.signup = (req, res, next) => {
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const email = req.body.email
    const password = req.body.password

    User.findOne({'email': email})
        .then(result => {
            if(result){
                res.render('signup', {
                    message: 'User with same email already exists...'
                })
            }
            const user = new User({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            })
            return user.save()
        })
        .then(result => {
            console.log('user created...')
            res.redirect('/login')
        })
        .catch(error => {

            console.log(error)

            res.render('signup', {
                message: 'Invalid Signup...'
            })
        })
}

// Page Not Found
module.exports.get404 = (req, res, next) => {
    res.render('404')
}