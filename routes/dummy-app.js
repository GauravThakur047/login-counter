const express = require('express')

const router = express.Router()
const dummyApp = require('../controllers/dummy-app')

// login => GET
router.get('/login', dummyApp.getLogin)

// signup => GET
router.get('/signup', dummyApp.getSignup)

// login => POST
router.post('/login', dummyApp.login)

// signup => POST
router.post('/signup', dummyApp.signup)

// getHome => GET
router.get('/home', dummyApp.getHome)

// login => GET
router.get('/', dummyApp.getLogin)

// Page Not Found
router.use(dummyApp.get404)

module.exports = router