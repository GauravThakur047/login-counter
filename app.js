const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const dummyAppRouter = require('./routes/dummy-app')
const app = express()
const MONGO_URI = 'mongodb+srv://Gaurav:Gsl5QgJKtxzH2ytK@cluster0-sghfc.mongodb.net/test?retryWrites=true&w=majority'

app.set('view engine', 'ejs')
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: false }))

app.use(dummyAppRouter)

mongoose.connect(MONGO_URI)
    .then(result => {
        console.log('[#] Connected to db')
        app.listen(process.env.PORT)
        // console.log(`[#] Server listening at port {process.env.PORT}`)
    })
    .catch(error => {
        console.log(error)
    })